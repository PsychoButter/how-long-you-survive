var ctx = document.getElementById("ctx").getContext("2d");

//canvas setting
ctx.font = '30px Arial';
ctx.fillStyle = 'white';

//global var
var HEI = 600;
var WID = 1000;
var frameCount = 0;
var Score = 0;
var HighScore = 0;
var lifeTime = 0;
var timeWhenGameStarted = Date.now();
var refresh;

//List
var listEnemy = {};
var listBonus = {};
var listHeal = {};

//Player setting
var player = {
  color: 'green',
  hp: 50,
  wid: 15,
  hei: 15,
  x: 235, //start player position x
  y: 235, //start player position y
};

//control
document.onmousemove = function(mouse){
  var mouseX = mouse.clientX - document.getElementById('ctx').getBoundingClientRect().left;
  var mouseY = mouse.clientY - document.getElementById('ctx').getBoundingClientRect().top;
//repair player position without canvas
if(mouseX < player.wid/2){
  mouseX = player.wid/2;
}
if(mouseX > WID - player.wid/2){
  mouseX = WID - player.wid/2;
}
if(mouseY < player.hei/2){
  mouseY = player.hei/2;
}
if(mouseY > HEI - player.hei/2){
  mouseY = HEI - player.hei/2;
}
player.x = mouseX;
player.y = mouseY;
}
//restart
document.onkeydown = function(e){
  var keyCode = e.keyCode;
  if(keyCode == 82){
    if(refresh == null){
      refresh = setInterval(update, 20);
    }else{
      clearInterval(refresh);
      refresh = setInterval(update, 20);
    }

    newGame();
  }
}
//Enemy
function Enemy(id,wid,hei,x,y,spdx,spdy) {
  var enemy = {
    color: 'red',
    id:id,
    wid:wid,
    hei:hei,
    x:x,
    y:y,
    spdx:spdx,
    spdy:spdy,
  };
//add to enemy list
  listEnemy[id] = enemy;
}
//Bonus
function Bonus(id,wid,hei,x,y) {
  var bonus = {
    color: 'blue',
    id:id,
    wid:wid,
    hei:hei,
    x:x,
    y:y,
    spdx:0,
    spdy:0,
  };
//add to bonus list
  listBonus[id] = bonus;
}
//heal
function Heal(id,wid,hei,x,y) {
  var heal = {
    color: 'yellow',
    id:id,
    wid:wid,
    hei:hei,
    x:x,
    y:y,
    spdx:0,
    spdy:0,
  };
//add to bonus list
  listHeal[id] = heal;
}
//test collision
function testCollisionRectRect(rect1,rect2){
  return rect1.x <= rect2.x+rect2.wid
    && rect2.x <= rect1.x+rect1.wid
    && rect1.y <= rect2.y + rect2.hei
    && rect2.y <= rect1.y + rect1.hei;
}

function testCollisionEntity(entity1,entity2) {
  var rect1 = {
    x:entity1.x-entity1.wid/2,
    y:entity1.y-entity1.hei/2,
    wid:entity1.wid,
    hei:entity1.hei,
  }
  var rect2 = {
    x:entity2.x-entity2.wid/2,
    y:entity2.y-entity2.hei/2,
    wid:entity2.wid,
    hei:entity2.hei,
  }
  return testCollisionRectRect(rect1,rect2);
}
//update Entity
function update_Entity(entity){
  update_Entity_Pos(entity);
  draw_Entity(entity);
}
//update Entity Position
function update_Entity_Pos(entity) {
  entity.x += entity.spdx;
  entity.y += entity.spdy;

  if (entity.x > WID || entity.x < 0) {
    entity.spdx = -entity.spdx;
  }
  if (entity.y > HEI || entity.y < 0) {
    entity.spdy = -entity.spdy;
  }
}
//draw Entity
draw_Entity = function (entity){
  ctx.save();
  ctx.fillStyle = entity.color;
  ctx.fillRect(entity.x-entity.wid/2,entity.y-entity.hei/2,entity.wid,entity.hei);
  ctx.restore();
}
//enemy Generator
function enemyGenerator() {
  var id = Math.random();
  var hei = 10 + Math.random()*30;
  var wid = 10 + Math.random()*30;
  var x = Math.random()*WID;
  var y = Math.random()*HEI;
  var spdx = 4 + Math.random()*4;
  var spdy = 4 + Math.random()*4;
  Enemy(id,wid,hei,x,y,spdx,spdy)
}
//bonus Generator
function bonusGenerator() {
  var id = Math.random();
  var hei = 5;
  var wid = 5;
  var x = Math.random()*WID;
  var y = Math.random()*HEI;
  Bonus(id,wid,hei,x,y)
}
//heal Generator
function healGenerator() {
  var id = Math.random();
  var hei = 5;
  var wid = 5;
  var x = Math.random()*WID;
  var y = Math.random()*HEI;
  Heal(id,wid,hei,x,y)
}
startMenu();
//start menu
function startMenu() {
  ctx.textAlign='center';
  ctx.fillText("How long you survive ?",WID/2,HEI/2);
  ctx.font = '15px Arial';
  ctx.fillText("Press 'R' to start play",WID/2,HEI/2+30);
  ctx.font = '30px Arial';
  load();
}

//new game
function newGame() {
  player.hp = 50;
  Score = 0;
  frameCount = 0;
  lifeTime = 0;
  timeWhenGameStarted = Date.now();
  listEnemy = {};
  listBonus = {};
  listHeal = {};
  enemyGenerator();
  enemyGenerator();
  enemyGenerator();
  enemyGenerator();
  refresh;
}
//end game
function endGame() {
  clearInterval(refresh);
  if(Score > HighScore){
    HighScore = Score;
  }
  ctx.clearRect(0,0,WID,HEI);
  ctx.textAlign='center';
  ctx.fillText("HighScore: " +HighScore,WID/2,HEI/2+100);
  ctx.fillText("scored " +Score+" points in "+lifeTime+" seconds",WID/2,HEI/2);
  ctx.font = '10px Arial';
  ctx.fillText("Press 'R' to restart",WID/2,HEI/2+50);
  ctx.font = '30px Arial';
  save();
}

//update
update = function(){
//clear canvas
  ctx.clearRect(0,0,WID,HEI);
//add item at refresh
  frameCount++;
  if (frameCount % 20 === 0) {
    Score++;
  }
  if(frameCount % 200 === 0){
    enemyGenerator();
  }
  if(frameCount % 500 === 0){
    bonusGenerator();
  }
  if(frameCount % 50 === 0){
    lifeTime++;
  }
  if(frameCount % 1000 === 0){
    healGenerator();
  }
//check bonus collision with player
  for(var key in listBonus){
    update_Entity(listBonus[key]);
    var isColliding = testCollisionEntity(player,listBonus[key]);
    if(isColliding){
      Score+=100;
      delete listBonus[key];
    }
  }
//check colision player with enemy
  for(var key in listEnemy){
    update_Entity(listEnemy[key]);
    var isColliding = testCollisionEntity(player,listEnemy[key]);
    if(isColliding) {
      player.hp = player.hp - 1;
      }
    }
    for(var key in listHeal){
      update_Entity(listHeal[key]);
      var isColliding = testCollisionEntity(player,listHeal[key]);
      if(isColliding){
        player.hp += 5;
        delete listHeal[key];
      }
    }
//if player dead
  if(player.hp <= 0){
    endGame();
  }
  draw_Entity(player);
  ctx.textAlign='left';
  ctx.fillText(player.hp + 'hp',3,30);
  ctx.textAlign='center';
  ctx.fillText('Score: '+Score,WID/2,30);
  ctx.textAlign='right';
  ctx.fillText('HighScore: '+HighScore,990,30);
}
function save() {
  localStorage.setItem("highscore",HighScore);
}
function load() {
  highscore = localStorage.getItem("highscore");
  if (highscore == null) {
    HighScore = 0;
  }else{
      HighScore = parseInt(highscore);
  }

}
